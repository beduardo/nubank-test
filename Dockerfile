FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /buildapp
COPY ./authorizer-service ./authorizer-service
COPY ./authorizer-service-tests ./authorizer-service-tests
COPY ./authorizer.sln ./authorizer.sln
RUN dotnet restore
RUN dotnet build -c Debug --no-restore
RUN dotnet test --no-build
RUN dotnet build -c Release --no-restore
RUN dotnet publish -o publish -c Release --no-build --no-restore

FROM microsoft/dotnet:2.2-runtime AS runtime
COPY --from=build /buildapp/authorizer-service/publish .
CMD dotnet authorizer-service.dll info