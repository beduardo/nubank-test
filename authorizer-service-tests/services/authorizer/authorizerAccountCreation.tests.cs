using System;
using authorizer_service.domain;
using authorizer_service.services;
using FluentAssertions;
using Xunit;

namespace authorizer_service_tests.services.authorizer
{
    public class AuthorizerAccountCreationTests
    {
        private Authorizer createInstance() {
            return new Authorizer();
        }

        [Fact]
        public void CreateAuthorizer() {
            var instance = createInstance();
            instance.Should().NotBeNull();
        }

        [Theory]
        [InlineData(true, 100)]
        [InlineData(true, 80)]
        [InlineData(false, 30)]
        public void AccountCreationOnce_ReturnCorrectOperationResult(bool activeStatus, int availableLimit) {
            var instance = createInstance();

            var acOp = new OperationAccountCreation {
                ActiveCard = activeStatus,
                AvailableLimit = availableLimit
            };

            var ret = instance.Execute(acOp);

            ret.Should().NotBeNull();
            ret.Should().BeEquivalentTo(new OperationResult {
                account = new OperationResultAccount {
                    activeCard = activeStatus,
                    availableLimit = availableLimit
                },
                violations = new string[] { }
            });
        }

        [Theory]
        [InlineData(true, 100, true, 100)]
        [InlineData(true, 80, true, 80)]
        [InlineData(false, 30, true, 100)]
        [InlineData(true, 130, false, 40)]
        public void AccountCreationTwice_ReturnCorrectOperationsResult(bool activeStatus, int availableLimit, bool activeStatus2, int availableLimit2) {
            var instance = createInstance();

            var acOp = new OperationAccountCreation {
                ActiveCard = activeStatus,
                AvailableLimit = availableLimit
            };

            var acOp2 = new OperationAccountCreation {
                ActiveCard = activeStatus2,
                AvailableLimit = availableLimit2
            };

            instance.Execute(acOp);
            var ret = instance.Execute(acOp2);

            ret.Should().NotBeNull();
            ret.Should().BeEquivalentTo(new OperationResult {
                account = new OperationResultAccount {
                    activeCard = activeStatus,
                    availableLimit = availableLimit
                },
                violations = new string[] { "account-already-initialized" }
            });
        }

    }
}