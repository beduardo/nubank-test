using System;
using System.Collections.Generic;
using authorizer_service.domain;
using authorizer_service.services;
using FluentAssertions;
using Xunit;

namespace authorizer_service_tests.services.authorizer
{
    public class AuthorizerMultipleValidationsTests
    {

        List<OperationResult> retList;
        Authorizer instance;

        private void createInstance()
        {
            retList = new List<OperationResult>();
            instance = new Authorizer();
        }

        [Fact]
        public void CreateAuthorizer()
        {
            createInstance();
            instance.Should().NotBeNull();
        }

        private void createAccount(bool activeCard, int availableLimit)
        {
            retList.Add(instance.Execute(new OperationAccountCreation
            {
                ActiveCard = activeCard,
                AvailableLimit = availableLimit
            }));
        }

        private void executeTransaction(int amount, DateTimeOffset time)
        {
            retList.Add(instance.Execute(new OperationTransactionAuthorization
            {
                amount = amount,
                merchant = "AnyMerch",
                time = time
            }));
        }

        private void executeTransaction(string merchant, int amount, DateTimeOffset time)
        {
            retList.Add(instance.Execute(new OperationTransactionAuthorization
            {
                amount = amount,
                merchant = merchant,
                time = time
            }));
        }

        private OperationResult simulateResult(bool activeCard, int availableLimit, params string[] violations)
        {
            return new OperationResult
            {
                account = new OperationResultAccount
                {
                    activeCard = activeCard,
                    availableLimit = availableLimit
                },
                violations = violations
            };
        }

        [Fact]
        public void DoNotStopApplicationOnValidationError()
        {
            createInstance();

            createAccount(true, 100);

            executeTransaction(20, new DateTimeOffset(2019, 02, 04, 10, 0, 0, 0, TimeSpan.Zero));
            executeTransaction(90, new DateTimeOffset(2019, 02, 04, 11, 0, 0, 0, TimeSpan.Zero));
            executeTransaction(70, new DateTimeOffset(2019, 02, 04, 11, 0, 0, 0, TimeSpan.Zero));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 100),
                simulateResult(true, 80),
                simulateResult(true, 80, "insufficient-limit"),
                simulateResult(true, 10)
            },  opt => opt.WithStrictOrdering());
        }

        [Fact]
        public void SeveralValidationsInLine_CaseA()
        {
            createInstance();

            createAccount(true, 1000);

            executeTransaction("merch01", 20, new DateTimeOffset(2019, 02, 04, 10, 0, 0, 0, TimeSpan.Zero)); //980
            executeTransaction("merch02", 90, new DateTimeOffset(2019, 02, 04, 10, 5, 0, 0, TimeSpan.Zero)); //890
            executeTransaction("merch03", 70, new DateTimeOffset(2019, 02, 04, 10, 10, 0, 0, TimeSpan.Zero)); //820
            executeTransaction("merch04", 900, new DateTimeOffset(2019, 02, 04, 10, 15, 0, 0, TimeSpan.Zero)); //insufficient-limit
            executeTransaction("merch05", 90, new DateTimeOffset(2019, 02, 04, 10, 20, 0, 0, TimeSpan.Zero)); //730
            executeTransaction("merch06", 120, new DateTimeOffset(2019, 02, 04, 10, 20, 30, 0, TimeSpan.Zero)); //610
            executeTransaction("merch07", 10, new DateTimeOffset(2019, 02, 04, 10, 21, 0, 0, TimeSpan.Zero)); //600
            executeTransaction("merch08", 50, new DateTimeOffset(2019, 02, 04, 10, 21, 20, 0, TimeSpan.Zero)); //high-frequency-small-interval
            executeTransaction("merch09", 50, new DateTimeOffset(2019, 02, 04, 10, 30, 0, 0, TimeSpan.Zero)); //550
            executeTransaction("merch09", 100, new DateTimeOffset(2019, 02, 04, 10, 35, 0, 0, TimeSpan.Zero)); //450
            executeTransaction("merch09", 100, new DateTimeOffset(2019, 02, 04, 10, 36, 0, 0, TimeSpan.Zero)); //doubled-transaction
            executeTransaction("merch10", 100, new DateTimeOffset(2019, 02, 04, 10, 36, 30, 0, TimeSpan.Zero)); //350
            executeTransaction("merch10", 100, new DateTimeOffset(2019, 02, 04, 10, 39, 30, 0, TimeSpan.Zero)); //250


            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 1000),
                simulateResult(true, 980),
                simulateResult(true, 890),
                simulateResult(true, 820),
                simulateResult(true, 820, "insufficient-limit"),
                simulateResult(true, 730),
                simulateResult(true, 610),
                simulateResult(true, 600),
                simulateResult(true, 600, "high-frequency-small-interval"),
                simulateResult(true, 550),
                simulateResult(true, 450),
                simulateResult(true, 450, "doubled-transaction"),
                simulateResult(true, 350),
                simulateResult(true, 250)
            },  opt => opt.WithStrictOrdering());

        }

        [Fact]
        public void SeveralValidationsInLine_CaseB()
        {
            createInstance();

            createAccount(true, 1000);

            executeTransaction("merch01", 10, new DateTimeOffset(2019, 02, 04, 10, 0, 0, 0, TimeSpan.Zero)); //990
            executeTransaction("merch02", 10, new DateTimeOffset(2019, 02, 04, 10, 0, 10, 0, TimeSpan.Zero)); //980
            executeTransaction("merch03", 10, new DateTimeOffset(2019, 02, 04, 10, 0, 20, 0, TimeSpan.Zero)); //970
            executeTransaction("merch04", 1000, new DateTimeOffset(2019, 02, 04, 10, 0, 30, 0, TimeSpan.Zero)); //insufficient-limit, high-frequency-small-interval
            executeTransaction("merch05", 10, new DateTimeOffset(2019, 02, 04, 10, 20, 0, 0, TimeSpan.Zero)); //960
            executeTransaction("merch06", 10, new DateTimeOffset(2019, 02, 04, 10, 20, 30, 0, TimeSpan.Zero)); //950
            executeTransaction("merch07", 10, new DateTimeOffset(2019, 02, 04, 10, 20, 40, 0, TimeSpan.Zero)); //940
            executeTransaction("merch07", 10, new DateTimeOffset(2019, 02, 04, 10, 20, 50, 0, TimeSpan.Zero)); //high-frequency-small-interval, doubled-transaction
            executeTransaction("merch09", 10, new DateTimeOffset(2019, 02, 04, 10, 30, 0, 0, TimeSpan.Zero)); //930
            executeTransaction("merch09", 500, new DateTimeOffset(2019, 02, 04, 10, 35, 0, 0, TimeSpan.Zero)); //430
            executeTransaction("merch09", 500, new DateTimeOffset(2019, 02, 04, 10, 36, 0, 0, TimeSpan.Zero)); //insufficient-limit, doubled-transaction
            executeTransaction("merch10", 10, new DateTimeOffset(2019, 02, 04, 10, 40, 30, 0, TimeSpan.Zero)); //420
            executeTransaction("merch11", 10, new DateTimeOffset(2019, 02, 04, 10, 40, 40, 0, TimeSpan.Zero)); //410
            executeTransaction("merch12", 300, new DateTimeOffset(2019, 02, 04, 10, 40, 50, 0, TimeSpan.Zero)); //110
            executeTransaction("merch12", 300, new DateTimeOffset(2019, 02, 04, 10, 41, 0, 0, TimeSpan.Zero)); //insufficient-limit, high-frequency-small-interval, doubled-transaction
            executeTransaction("merch13", 10, new DateTimeOffset(2019, 02, 04, 10, 50, 30, 0, TimeSpan.Zero)); //100


            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 1000),
                simulateResult(true, 990),
                simulateResult(true, 980),
                simulateResult(true, 970),
                simulateResult(true, 970, "insufficient-limit", "high-frequency-small-interval"),
                simulateResult(true, 960),
                simulateResult(true, 950),
                simulateResult(true, 940),
                simulateResult(true, 940, "high-frequency-small-interval", "doubled-transaction"),
                simulateResult(true, 930),
                simulateResult(true, 430),
                simulateResult(true, 430, "insufficient-limit", "doubled-transaction"),
                simulateResult(true, 420),
                simulateResult(true, 410),
                simulateResult(true, 110),
                simulateResult(true, 110, "insufficient-limit", "high-frequency-small-interval", "doubled-transaction"),
                simulateResult(true, 100)
            },  opt => opt.WithStrictOrdering());

        }

        [Fact]
        public void CardInactiveInLine()
        {
            createInstance();

            createAccount(false, 1000);

            executeTransaction("merch01", 10, new DateTimeOffset(2019, 02, 04, 10, 0, 0, 0, TimeSpan.Zero)); //card-not-active
            executeTransaction("merch02", 10, new DateTimeOffset(2019, 02, 04, 10, 5, 0, 0, TimeSpan.Zero)); //card-not-active
            executeTransaction("merch03", 10, new DateTimeOffset(2019, 02, 04, 10, 10, 0, 0, TimeSpan.Zero)); //card-not-active
            executeTransaction("merch04", 1000, new DateTimeOffset(2019, 02, 04, 10, 15, 0, 0, TimeSpan.Zero)); //card-not-active
        
            retList.Should().BeEquivalentTo(new[] {
                simulateResult(false, 1000),
                simulateResult(false, 1000, "card-not-active"),
                simulateResult(false, 1000, "card-not-active"),
                simulateResult(false, 1000, "card-not-active"),
                simulateResult(false, 1000, "card-not-active")
            },  opt => opt.WithStrictOrdering());

        }

    }
}