using System;
using System.Collections.Generic;
using authorizer_service.domain;
using authorizer_service.services;
using FluentAssertions;
using Xunit;

namespace authorizer_service_tests.services.authorizer
{
    public class AuthorizerTransactionAuthorizationTests
    {
        private Authorizer createInstance(bool activeCard, int availableLimit)
        {
            var instance = new Authorizer();
            instance.Execute(new OperationAccountCreation
            {
                ActiveCard = activeCard,
                AvailableLimit = availableLimit
            });
            return instance;
        }

        private OperationResult executeTransaction(Authorizer instance, int amount, DateTimeOffset time) {
            return instance.Execute(new OperationTransactionAuthorization
            {
                amount = amount,
                merchant = "AnyMerch",
                time = time
            });
        }

        private OperationResult executeTransaction(Authorizer instance, string merchant, int amount, DateTimeOffset time) {
            return instance.Execute(new OperationTransactionAuthorization
            {
                amount = amount,
                merchant = merchant,
                time = time
            });
        }

        private OperationResult simulateResult(bool activeCard, int availableLimit, params string[] violations) {
            return new OperationResult {
                account = new OperationResultAccount {
                    activeCard = activeCard,
                    availableLimit = availableLimit
                },
                violations = violations
            };
        }

        [Fact]
        public void CreateAuthorizer()
        {
            var instance = createInstance(true, 100);
            instance.Should().NotBeNull();
        }

        [Fact]
        public void ValidationAvailableLimit_CaseA_OneTransactionWithAvailableLimit()
        {
            var instance = createInstance(true, 100);

            var ret = executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero));
            
            ret.Should().BeEquivalentTo(simulateResult(true, 80));
        }

        [Fact]
        public void ValidationAvailableLimit_CaseB_TwoTransactionsWithNotAvailableLimit()
        {
            var instance = createInstance(true, 100);

            executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero));
            var ret = executeTransaction(instance, 90, new DateTimeOffset(2019, 02, 13, 11, 0, 0, 0, TimeSpan.Zero));

            ret.Should().BeEquivalentTo(simulateResult(true, 80, "insufficient-limit"));
        }

        [Fact]
        public void ValidationCardInactive_CaseA_CardInactive()
        {
            var instance = createInstance(false, 100);

            var ret = executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero));

            ret.Should().BeEquivalentTo(simulateResult(false, 100, "card-not-active"));
        }

        [Fact]
        public void ValidationHighFrequencySmallInterval_CaseA_FourthInvalid()
        {
            var instance = createInstance(true, 100);

            var retList = new List<OperationResult>();

            retList.Add(executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 10, new DateTimeOffset(2019, 02, 13, 10, 0, 45, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 12, new DateTimeOffset(2019, 02, 13, 10, 1, 10, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 25, new DateTimeOffset(2019, 02, 13, 10, 1, 50, 0, TimeSpan.Zero)));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 80),
                simulateResult(true, 70),
                simulateResult(true, 58),
                simulateResult(true, 58, "high-frequency-small-interval")
            },  opt => opt.WithStrictOrdering());
        }

        [Fact]
        public void ValidationHighFrequencySmallInterval_CaseB_FifthValid()
        {
            var instance = createInstance(true, 100);

            var retList = new List<OperationResult>();
            
            retList.Add(executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 10, new DateTimeOffset(2019, 02, 13, 10, 0, 45, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 12, new DateTimeOffset(2019, 02, 13, 10, 1, 10, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 25, new DateTimeOffset(2019, 02, 13, 10, 1, 50, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 25, new DateTimeOffset(2019, 02, 13, 10, 2, 10, 0, TimeSpan.Zero)));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 80),
                simulateResult(true, 70),
                simulateResult(true, 58),
                simulateResult(true, 58, "high-frequency-small-interval"),
                simulateResult(true, 33)
            },  opt => opt.WithStrictOrdering());
        }

        [Fact]
        public void ValidationHighFrequencySmallInterval_CaseC_FifthInvalid()
        {
            var instance = createInstance(true, 100);

            var retList = new List<OperationResult>();
            
            retList.Add(executeTransaction(instance, 25, new DateTimeOffset(2019, 02, 13, 9, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 10, new DateTimeOffset(2019, 02, 13, 10, 0, 45, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 12, new DateTimeOffset(2019, 02, 13, 10, 1, 10, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, 25, new DateTimeOffset(2019, 02, 13, 10, 1, 50, 0, TimeSpan.Zero)));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 75),
                simulateResult(true, 55),
                simulateResult(true, 45),
                simulateResult(true, 33),
                simulateResult(true, 33, "high-frequency-small-interval"),
            },  opt => opt.WithStrictOrdering());
        }

        [Fact]
        public void ValidationDoubledTransaction_CaseA_SecondInvalid()
        {
            var instance = createInstance(true, 100);

            var retList = new List<OperationResult>();

            retList.Add(executeTransaction(instance, "merch01", 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, "merch01", 20, new DateTimeOffset(2019, 02, 13, 10, 0, 25, 0, TimeSpan.Zero)));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 80),
                simulateResult(true, 80, "doubled-transaction")
            },  opt => opt.WithStrictOrdering());
        }

        [Fact]
        public void ValidationDoubledTransaction_CaseB_DifferentMerchs_ThirdInvalid()
        {
            var instance = createInstance(true, 100);

            var retList = new List<OperationResult>();

            retList.Add(executeTransaction(instance, "merch01", 20, new DateTimeOffset(2019, 02, 13, 10, 0, 0, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, "merch02", 20, new DateTimeOffset(2019, 02, 13, 10, 0, 25, 0, TimeSpan.Zero)));
            retList.Add(executeTransaction(instance, "merch02", 20, new DateTimeOffset(2019, 02, 13, 10, 1, 25, 0, TimeSpan.Zero)));

            retList.Should().BeEquivalentTo(new[] {
                simulateResult(true, 80),
                simulateResult(true, 60),
                simulateResult(true, 60, "doubled-transaction")
            },  opt => opt.WithStrictOrdering());
        }
    }
}