
using authorizer_service.services;
using Xunit;
using FluentAssertions;
using authorizer_service.util;
using Moq;
using authorizer_service.domain;
using System.Collections.Generic;

namespace authorizer_service_tests.services
{
    public class InterfaceCliTests
    {
        Mock<IConsoleFacade> mockConsoleFacade;
        Mock<IOperationParser> mockOperationParser;
        Mock<IAuthorizer> mockAuthorizer;

        private InterfaceCli createInstance() {
            mockConsoleFacade = new Mock<IConsoleFacade>();
            mockOperationParser = new Mock<IOperationParser>();
            mockAuthorizer = new Mock<IAuthorizer>();
            return new InterfaceCli(mockConsoleFacade.Object, mockOperationParser.Object, mockAuthorizer.Object);
        }

        [Fact]
        public void CreateInterfaceCli() {
            var instance = createInstance();
            instance.Should().NotBeNull();
        }

        [Fact]
        public void showInfo_PrintHelpOnConsole() {
            var instance = createInstance();
            instance.showInfo();
            mockConsoleFacade.Verify(m => m.PrintLine("To run send inputs in stdin with `< filename`"));
        }

        [Fact]
        public void getStd_SendLinesToParser_ExampleA() {
            var instance = createInstance();

            mockConsoleFacade.SetupSequence(m => m.ReadNextLine())
                .Returns("Line A")
                .Returns("Line B")
                .Returns("Line C")
                .Returns((string)null);


            instance.getStdIn();

            mockConsoleFacade.Verify(m => m.ReadNextLine(), Times.Exactly(4));
            mockOperationParser.Verify(m => m.Parse("Line A"));
            mockOperationParser.Verify(m => m.Parse("Line B"));
            mockOperationParser.Verify(m => m.Parse("Line C"));
            mockOperationParser.Verify(m => m.Parse(It.IsAny<string>()), Times.Exactly(3));
        }

        [Fact]
        public void getStd_SendLinesToParser_ExampleB() {
            var instance = createInstance();

            mockConsoleFacade.SetupSequence(m => m.ReadNextLine())
                .Returns("Line 1")
                .Returns("Line 2")
                .Returns("Line 3")
                .Returns((string)null);


            instance.getStdIn();

            mockConsoleFacade.Verify(m => m.ReadNextLine(), Times.Exactly(4));
            mockOperationParser.Verify(m => m.Parse("Line 1"));
            mockOperationParser.Verify(m => m.Parse("Line 2"));
            mockOperationParser.Verify(m => m.Parse("Line 3"));
            mockOperationParser.Verify(m => m.Parse(It.IsAny<string>()), Times.Exactly(3));
        }

        [Fact]
        public void getStd_SendParsedOperationsToAuthorizerWithCorrectOrder() {
            var instance = createInstance();
            var executionOrder = new List<int>();
            mockConsoleFacade.SetupSequence(m => m.ReadNextLine())
                .Returns("Line A")
                .Returns("Line B")
                .Returns("Line C")
                .Returns((string)null);

            var op1 = new Operation();
            var op2 = new Operation();
            var op3 = new Operation();
            mockOperationParser.Setup(m => m.Parse("Line A")).Returns(op1);
            mockOperationParser.Setup(m => m.Parse("Line B")).Returns(op2);
            mockOperationParser.Setup(m => m.Parse("Line C")).Returns(op3);

            mockAuthorizer.Setup(m => m.Execute(op1)).Callback(() => executionOrder.Add(1));
            mockAuthorizer.Setup(m => m.Execute(op2)).Callback(() => executionOrder.Add(2));
            mockAuthorizer.Setup(m => m.Execute(op3)).Callback(() => executionOrder.Add(3));

            instance.getStdIn();

            mockAuthorizer.Verify(m => m.Execute(op1));
            mockAuthorizer.Verify(m => m.Execute(op2));
            mockAuthorizer.Verify(m => m.Execute(op3));
            mockAuthorizer.VerifyNoOtherCalls();
            executionOrder.Should().BeEquivalentTo(new[] { 1, 2, 3});
        }

        [Fact]
        public void getStd_SendParsedOperationResultsToSerialize() {
            var instance = createInstance();
            mockConsoleFacade.SetupSequence(m => m.ReadNextLine())
                .Returns("Line A")
                .Returns("Line B")
                .Returns("Line C")
                .Returns((string)null);

            var op1 = new OperationResult();
            var op2 = new OperationResult();
            var op3 = new OperationResult();
            mockAuthorizer.SetupSequence(m => m.Execute(It.IsAny<Operation>()))
                .Returns(op1)
                .Returns(op2)
                .Returns(op3);

            instance.getStdIn();

            mockOperationParser.Verify(m => m.Serialize(op1));
            mockOperationParser.Verify(m => m.Serialize(op2));
            mockOperationParser.Verify(m => m.Serialize(op2));
            mockOperationParser.Verify(m => m.Serialize(It.IsAny<OperationResult>()), Times.Exactly(3));
        }

        [Fact]
        public void getStd_PrintParserSerializeReturnToConsole() {
            var instance = createInstance();
            mockConsoleFacade.SetupSequence(m => m.ReadNextLine())
                .Returns("Line A")
                .Returns("Line B")
                .Returns("Line C")
                .Returns((string)null);

            mockOperationParser.SetupSequence(m => m.Serialize(It.IsAny<OperationResult>()))
                .Returns("Return 1")
                .Returns("Return 2")
                .Returns("Return 3");

            instance.getStdIn();

            mockConsoleFacade.Verify(m => m.PrintLine("Return 1"));
            mockConsoleFacade.Verify(m => m.PrintLine("Return 2"));
            mockConsoleFacade.Verify(m => m.PrintLine("Return 3"));
            mockConsoleFacade.Verify(m => m.PrintLine(It.IsAny<string>()), Times.Exactly(3));
        }
    }
}