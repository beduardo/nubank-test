
using authorizer_service.services;
using Xunit;
using FluentAssertions;
using Moq;
using authorizer_service.domain;
using System.Collections.Generic;
using System;

namespace authorizer_service_tests.services
{
    public class OperationParserTests {

        private OperationParser createInstance() {
            return new OperationParser();
        }

        [Fact]
        public void CreateOperationParser() {
            var instance = createInstance();

            instance.Should().NotBeNull();
        }

        [Fact]
        public void ParseAccountCreationOperation_ReturnCorrectlyTypedOperationObject() {

            var instance = createInstance();

            var jsonToParse = $"{{ 'account': {{ 'activeCard': true, 'availableLimit': 100 }} }}";

            var operation = instance.Parse(jsonToParse);

            operation.Should().NotBeNull();
            operation.Should().BeOfType<OperationAccountCreation>();
        }

        [Theory]
        [InlineData(true, 100)]
        [InlineData(false, 100)]
        [InlineData(true, 180)]
        [InlineData(false, 110)]
        public void ParseAccountCreationOperation_ReturnCorrectOperationObject(bool active, int availableLimit) {

            var instance = createInstance();

            var jsonToParse = $"{{ 'account': {{ 'activeCard': {active.ToString().ToLower()}, 'availableLimit': {availableLimit} }} }}";

            OperationAccountCreation operation = instance.Parse(jsonToParse) as OperationAccountCreation;

            operation.ActiveCard.Should().Be(active);
            operation.AvailableLimit.Should().Be(availableLimit);
        }

        [Fact]
        public void ParseTransactionAuthorizationOperation_ReturnCorrectlyTypedOperationObject() {

            var instance = createInstance();

            var jsonToParse = $"{{ 'transaction': {{ 'merchant': 'Burger King', 'amount': 20, 'time': '2019-02-13T10:00:00.000Z' }} }}";

            var operation = instance.Parse(jsonToParse);

            operation.Should().NotBeNull();
            operation.Should().BeOfType<OperationTransactionAuthorization>();
        }

        [Theory]
        [InlineData("Burger King", 20, "2019-02-13T10:00:00.000Z")]
        [InlineData("Habbibs", 90, "2019-02-13T11:00:00.000Z")]
        public void ParseTransactionAuthorizationOperation_ReturnCorrectOperationObject(string merchant, int amount, string time) {

            var instance = createInstance();

            var jsonToParse = $"{{ 'transaction': {{ 'merchant': '{merchant}', 'amount': {amount}, 'time': '{time}' }} }}";

            OperationTransactionAuthorization operation = instance.Parse(jsonToParse) as OperationTransactionAuthorization;

            operation.merchant.Should().Be(merchant);
            operation.amount.Should().Be(amount);
            operation.time.Should().Be(DateTimeOffset.Parse(time));
        }

        [Fact]
        public void SerializeOperationResult_ReturnCorrectJson_ExampleA() {
            var instance = createInstance();

            var opRes = new OperationResult {
                account = new OperationResultAccount {
                    activeCard = true, 
                    availableLimit = 100
                },
                violations = new string[] { }
            };

            var res = instance.Serialize(opRes);
            res.Should().Be(@"{""account"":{""activeCard"":true,""availableLimit"":100},""violations"":[]}");
        }

        [Fact]
        public void SerializeOperationResult_ReturnCorrectJson_ExampleB() {
            var instance = createInstance();

            var opRes = new OperationResult {
                account = new OperationResultAccount {
                    activeCard = true, 
                    availableLimit = 80
                },
                violations = new string[] { "insufficient-limit" }
            };

            var res = instance.Serialize(opRes);
            res.Should().Be(@"{""account"":{""activeCard"":true,""availableLimit"":80},""violations"":[""insufficient-limit""]}");
        }


    }
}