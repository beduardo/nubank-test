using authorizer_service;
using authorizer_service.services;
using Moq;
using Xunit;

namespace authorizer_service_tests.startup
{
    public class ProgramTests {
        [Theory]
        [InlineData("info")]
        [InlineData("INFO")]
        [InlineData("Info")]
        public void ParameterInfo_CallShowInfoOnly(string par) {
            var mockCli = new Mock<IInterfaceCli>();
            
            Program._instance = mockCli.Object;

            var argsToSend = new[] { par };

            Program.Main(argsToSend);

            mockCli.Verify(m => m.showInfo());
            mockCli.VerifyNoOtherCalls();
        }

        [Fact]
        public void AnyOtherParameter_CallGetStdIn() {
            var mockCli = new Mock<IInterfaceCli>();
            
            Program._instance = mockCli.Object;

            var argsToSend = new[] { "other" };

            Program.Main(argsToSend);

            mockCli.Verify(m => m.getStdIn());
        }

        [Fact]
        public void NoParameter_CallGetStdIn() {
            var mockCli = new Mock<IInterfaceCli>();
            
            Program._instance = mockCli.Object;

            var argsToSend = new string[] {};

            Program.Main(argsToSend);

            mockCli.Verify(m => m.getStdIn());
        }

        [Fact]
        public void NullArgs_CallGetStdIn() {
            var mockCli = new Mock<IInterfaceCli>();
            
            Program._instance = mockCli.Object;

            Program.Main(null);

            mockCli.Verify(m => m.getStdIn());
        }
    }
    
}