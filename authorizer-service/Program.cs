﻿using System;
using System.Linq;
using authorizer_service.services;
using authorizer_service.util;

namespace authorizer_service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args?.Length > 0 && args[0].ToLower() == "info") {
                GetInterfaceCli().showInfo();
            } else {
                GetInterfaceCli().getStdIn();
            }
        }
        
        public static IInterfaceCli _instance;
        public static IInterfaceCli GetInterfaceCli() {
            if (_instance == null) {
                _instance = new InterfaceCli(new ConsoleFacade(), new OperationParser(), new Authorizer());
            }
            return _instance;
        }
    }

}
