using System;

namespace authorizer_service.domain
{
    public class Operation
    {
    }

    public class OperationAccountCreation : Operation {
        public bool ActiveCard { get; set; }
        public int AvailableLimit { get; set; }
    }

    public class OperationTransactionAuthorization : Operation {
        public string merchant { get; set; }
        public int amount { get; set; }
        public DateTimeOffset time { get; set; }
    }
}

