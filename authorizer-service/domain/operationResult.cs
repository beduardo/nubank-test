namespace authorizer_service.domain
{
    public class OperationResult
    {
        public OperationResultAccount account { get; set; }
        public string[] violations { get; set; }
    }

    public class OperationResultAccount {
        public bool activeCard { get; set; }
        public int availableLimit { get; set; }
    }
}

