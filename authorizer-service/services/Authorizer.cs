using System;
using System.Collections.Generic;
using authorizer_service.domain;

namespace authorizer_service.services
{
    public interface IAuthorizer
    {
        OperationResult Execute(Operation operation);
    }

    public class Authorizer : IAuthorizer
    {
        private bool accountCreated;
        private int availableLimit;
        private bool activeCard;
        private DateTimeOffset lastSuccessTime = DateTimeOffset.MinValue;

        private TimeSpan highFrequencyOffset = new TimeSpan(0, 2, 0);
        private int transactionsCountInOffset = 0;
        private OperationTransactionAuthorization lastOperation;

        private TimeSpan doubleTransactionOffset = new TimeSpan(0, 2, 0);

        public OperationResult Execute(Operation operation)
        {
            var opTransaction = operation as OperationTransactionAuthorization;
            if (opTransaction != null)
            {
                return ExecuteTransactionAuthorization(opTransaction);
            }

            var opCreation = operation as OperationAccountCreation;
            if (opCreation != null)
            {
                return ExecuteAccountCreation(opCreation);
            }

            return null;
        }

        private OperationResult ExecuteAccountCreation(OperationAccountCreation opCreation)
        {
            if (accountCreated)
            {
                return CreateOperationResult(activeCard, availableLimit, "account-already-initialized");
            }
            accountCreated = true;
            activeCard = opCreation.ActiveCard;
            availableLimit = opCreation.AvailableLimit;
            return CreateOperationResult(activeCard, availableLimit);
        }

        private OperationResult ExecuteTransactionAuthorization(OperationTransactionAuthorization opTransaction)
        {
            bool authorize = true;
            var violations = new List<string>();

            //Verify if card is active
            if (!activeCard)
            {
                authorize = false;
                violations.Add("card-not-active");
            }

            //Verify if limit is sufficient
            if (availableLimit < opTransaction.amount)
            {
                authorize = false;
                violations.Add("insufficient-limit");
            }

            //Verify if the frequency is High
            var timeOffset = (opTransaction.time - lastSuccessTime);
            if (timeOffset < highFrequencyOffset)
            {
                if (transactionsCountInOffset == 3)
                {
                    authorize = false;
                    violations.Add("high-frequency-small-interval");
                }
                else
                {
                    transactionsCountInOffset++;
                }
            }
            else
            {
                transactionsCountInOffset = 0;
            }

            //Verify if is a doubled transaction
            if (timeOffset < doubleTransactionOffset &&
                lastOperation.amount == opTransaction.amount &&
                lastOperation.merchant == opTransaction.merchant)
            {
                authorize = false;
                violations.Add("doubled-transaction");
            }

            if (authorize)
            {
                availableLimit -= opTransaction.amount;
                if (transactionsCountInOffset == 0)
                {
                    lastSuccessTime = opTransaction.time;
                    transactionsCountInOffset = 1;
                }
                lastOperation = opTransaction;
            }

            return CreateOperationResult(activeCard, availableLimit, violations.ToArray());
        }

        private OperationResult CreateOperationResult(bool _activeCard, int _availableLimit, params string[] _violations)
        {
            return new OperationResult
            {
                account = new OperationResultAccount
                {
                    activeCard = _activeCard,
                    availableLimit = _availableLimit
                },
                violations = _violations
            };
        }

    }
}