using authorizer_service.util;

namespace authorizer_service.services
{
    public interface IInterfaceCli
    {
        void showInfo();
        void getStdIn();
    }

    public class InterfaceCli : IInterfaceCli
    {
        IConsoleFacade consoleFacade;
        IOperationParser operationParser;
        IAuthorizer authorizerService;
        public InterfaceCli(IConsoleFacade Console, IOperationParser Parser, IAuthorizer AuthorizerService)
        {
            consoleFacade = Console;
            operationParser = Parser;
            authorizerService = AuthorizerService;

        }

        public void getStdIn()
        {
            string line;
            while((line = consoleFacade.ReadNextLine()) != null) {
                var op = operationParser.Parse(line);
                var res = authorizerService.Execute(op);
                var ret = operationParser.Serialize(res);
                consoleFacade.PrintLine(ret);
            }
        }

        public void showInfo()
        {
            consoleFacade.PrintLine("To run send inputs in stdin with `< filename`");
        }
    }
}