using System.Xml;
using System.Xml.Linq;
using System.Linq;
using authorizer_service.domain;
using Newtonsoft.Json;
using System;

namespace authorizer_service.services
{
    public interface IOperationParser {
        Operation Parse(string line);  
        string Serialize(OperationResult operationResult); 
    }

    public class OperationParser : IOperationParser
    {
        public Operation Parse(string line)
        {
            XmlDocument parsed = JsonConvert.DeserializeXmlNode(line);
            XDocument xdoc = XDocument.Parse(parsed.OuterXml);

            XElement root = xdoc.Descendants("transaction").FirstOrDefault(); 
            if (root != null) {
                return ParseTransactionAuthorization(root);
            }

            root = xdoc.Descendants("account").FirstOrDefault();
            if (root != null) {
                return ParseAccountCreation(root);
            }


            return null;
        }

        private OperationAccountCreation ParseAccountCreation(XElement account) {

            XElement activeCardXml = account.Element("activeCard");
            bool activeCard = activeCardXml.Value.Equals("True", System.StringComparison.InvariantCultureIgnoreCase);

            XElement availableLimitXml = account.Element("availableLimit");
            int availableLimit = int.Parse(availableLimitXml.Value);

            return new OperationAccountCreation {
                ActiveCard = activeCard,
                AvailableLimit = availableLimit
            };
        }

        private OperationTransactionAuthorization ParseTransactionAuthorization(XElement account) {

            XElement merchantXml = account.Element("merchant");
            string merchant = merchantXml.Value;

            XElement amountXml = account.Element("amount");
            int amount = int.Parse(amountXml.Value);

            XElement timeXml = account.Element("time");
            DateTimeOffset time = DateTimeOffset.Parse(timeXml.Value); 

            return new OperationTransactionAuthorization {
                merchant = merchant,
                amount = amount,
                time = time
            };
        }

        public string Serialize(OperationResult operationResult)
        {
            return JsonConvert.SerializeObject(operationResult);
        }

    }

}