using System;

namespace authorizer_service.util
{
    public interface IConsoleFacade
    {
        string ReadNextLine();
        void PrintLine(string msg);
    }
    public class ConsoleFacade : IConsoleFacade
    {
        public void PrintLine(string msg)
        {
            Console.WriteLine(msg);
        }

        public string ReadNextLine()
        {
            return Console.ReadLine();
        }
    }
}