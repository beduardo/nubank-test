dotnet restore
dotnet build -c Debug --no-restore
dotnet test --no-build
dotnet build -c Release --no-restore
dotnet publish -o publish -c Release --no-build --no-restore